import React from "react"
import Header from "./Header";
import Preloader from "./Preloader";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import "../styles/Chat.css"

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      usersData: [],
      isDataReady: false,
      messageText: ''
    }
  }

  componentDidMount() {
    const url = this.props.url;
    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({
        usersData: data,
        isDataReady: true
      }))
      .catch(error => console.log(error))
  }

  updateText = (value) =>{
    this.setState({ messageText: value })
  }

  getInfoForHeader() {
    const data = this.state.usersData;
    const latestDate = new Date(Math.max(...data.map(d => new Date(d.createdAt))));
    const users = []
    for (let elem of data) {
      if (!users.includes(elem.user)) users.push(elem.user)
    }
    return {
      usersCount: users.length,
      messagesCount: data.length,
      lastMessageDate: `${latestDate.getDay()}.${latestDate.getMonth()}.${latestDate.getFullYear()} ${latestDate.getHours()}:${latestDate.getMinutes()}`
    }
  }

  render() {
    return (
      <div className='chat'>
        <Preloader isDataReady={this.state.isDataReady} />
        <Header usersData={this.getInfoForHeader()}/>
        <MessageList usersData={this.state.usersData} ownText={this.state.messageText}/>
        <MessageInput updateText={this.updateText} />
      </div>
    )
  }
}

export default Chat