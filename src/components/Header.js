//  TODO Header - отдельный компонент (.header), который содержит название чата (.header-title),
// количество пользователей (.header-users-count), количество сообщений (.header-messages-count), 
// дата последнего сообщения (.header-last-message-date) в формате dd.mm.yyyy hh:mm (Например: 15.02.2021 13:35)
import React from "react"
import "../styles/Header.css"

class Header extends React.Component {
 
  render() {
    return (
      <header className='header'>
        <h1 className='header-title'>MyChat</h1>
        <div className='header-users-count'>{this.props.usersData.usersCount} participants</div>
        <div className='header-messages-count'>{this.props.usersData.messagesCount}  messages</div>
        <div className='header-last-message-date'>last-message at {this.props.usersData.lastMessageDate}</div>
      </header>
    )
  }
}

export default Header