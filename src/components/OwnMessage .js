import React from "react"
import "../styles/OwnMessage.css"

class OwnMessage extends React.Component {
  
  edit() {

  }
  delete() {

  }
  render() {
    const date = new Date();
    return (
      <div className='own-message'>
        <p className='message-text'>{this.props.text}</p>
        <div className='addition-opt'>
          <span className='message-time'>{date.getHours() + ':' + date.getMinutes()}</span>
          <button className='message-edit'>Edit</button>
          <button className='message-delete'>Delete</button>
        </div>
      </div>
    )
  }
}

export default OwnMessage