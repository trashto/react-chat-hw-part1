import React from "react"
import "../styles/MessageInput.css"

class MessageInput extends React.Component {
  setText(e) {
    this.setState({
      text: e.target.value
    });
  }
 
  render() {
    return (
      <div className='message-input'>
        <textarea className='message-input-text' type='text' onChange={(e) => this.setText(e)} ></textarea>
        <button className='message-input-button' onClick={()=> this.props.updateText(this.state.text)}>Send</button>
      </div>
    )
  }
}


export default MessageInput