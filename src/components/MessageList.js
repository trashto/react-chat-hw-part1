import React from "react"
import "../styles/MessageList.css"
import Message from "./Message"
import OwnMessage from "./OwnMessage ";

class MessageList extends React.Component {
  renderOwnMessage(){
    if(this.props.ownText){
      return <OwnMessage text={this.props.ownText}/>
    }
    return
  }
  renderAllExternalMessages() {
    const data = this.props.usersData;
    data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    data.reverse()
    const messages = data.map((message, i) => <Message key={i} messageData={message}/>)
    return messages
  }
  render() {
    return (
      <div className='message-list'>
        {this.renderAllExternalMessages()}
        {this.renderOwnMessage()}
      </div>
    )
  }
}


export default MessageList