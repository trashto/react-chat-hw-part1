import React from "react";
import "../styles/Preloader.css"

class Preloader extends React.Component {

  displayPage() {
    if (this.props.isDataReady) {
      document.querySelector('body').style.visibility = 'visible';
      document.querySelector('.preloader').style.display = 'none';
    }
  }
  
  render() {
    return (
      <div className='preloader'>
        {this.displayPage()}
      </div>
    )
  }
}

export default Preloader